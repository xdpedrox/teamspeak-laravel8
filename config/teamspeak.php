<?php

return [
  'connection' => [
    'ip' => "eliteportuguesa.pt",
    'user' => "serveradmin",
    'pass' => "ka6aPx6C",
    'query_port' => 10011,
    'voice_port' => 9987,
    'display_name' => 'ElitePTNew',
  ],
  'config' => [
    'codec' => 0x04,
    'codec_quality' => 5,
    
    // cooldown em segundos quando se apaga canal principal
    'cooldown' => 10,
    
    // ID do separador a partir de onde começam os canais
    'user_channels_start' => 2,
    
    // ID do separador a partir de onde acabam os canais
    'user_channels_end' => 3,
    
    // ID do grupo que pode criar canais
    // FUNCIONA -> APENAS UM NUMERO -> ID de grupo a fazer whitelist OU 0 para TODOS
    // FUNCIONA -> ARRAY COM NUMEROS -> array(ID, ID, ID),
    'create_channels_group' => 0,
    
    // ID do channel admin
    'channel_admin_group' => 6,
    
    // limite de sub canais
    'sub_channel_limit' => 3
  ]
];

?>