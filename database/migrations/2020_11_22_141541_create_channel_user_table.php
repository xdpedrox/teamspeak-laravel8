<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChannelUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channel_tsuser', function (Blueprint $table) {
          $table->id();
          $table->bigInteger('tsuser_id')->unsigned();
          $table->bigInteger('channel_id')->unsigned();
          $table->integer('channel_group_id')->default(1);
          $table->timestamps();

          $table->foreign('tsuser_id')->references('id')->on('tsusers');
          $table->foreign('channel_id')->references('id')->on('channels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channel_tsuser');
    }
}
