<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TeamSpeakController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [TeamSpeakController::class, 'index']);

Route::post('/createChannel', [TeamSpeakController::class, 'createChannel'])->name('teamspeak.createChannel');
Route::delete('/deleteChannel', [TeamSpeakController::class, 'deleteChannel'])->name('teamspeak.deleteChannel');
Route::post('/createSubChannel', [TeamSpeakController::class, 'createSubChannel'])->name('teamspeak.createSubChannel');
Route::delete('/deleteSubChannel', [TeamSpeakController::class, 'deleteSubChannel'])->name('teamspeak.deleteSubChannel');
