<?php

namespace App\Helpers;

class ConnectedHelper
{
    static function currentUser()
    {
      return resolve('tsuser');
    }
    
    static function currentClient()
    {
      return resolve('tsclient');    
    }
}

?>