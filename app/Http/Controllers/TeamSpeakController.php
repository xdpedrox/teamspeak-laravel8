<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\TeamSpeakCore;
use App\Services\TeamSpeak;
use App\Models\Tsuser;
use App\Models\Channel;
// use Teamspeak3;
// use TeamSpeak3_Exception;

class TeamSpeakController extends Controller
{
  // use App\Pizza;

  protected $ts3;

  public function __construct(TeamSpeakCore $ts3core, TeamSpeak $ts3) {
    $this->ts3core = $ts3core;
    $this->ts3 = $ts3;
    $this->ts3lib = $ts3->ts3lib();
  }

  public function index() {
    $user = resolve('tsuser');

    $mainChannel = resolve('mainChannel');
    $channel = resolve('channel');

    if ($mainChannel) {
      return view('teamspeak.show', [
          'cid' => $mainChannel['cid'],
          'mainChannel' => $mainChannel,
          'subChannels' => $mainChannel->subChannelList()
        ]);
    }
    return view('teamspeak.create');
  }


  public function createChannel() {
    $params = request()->post();

    $user = resolve('tsuser');
    $client = resolve('tsclient');

    $spacerPrefix = '[cspacer' . $this->ts3->spacerCrc32($params['name'], time()) . ']';

    if (strlen(trim($spacerPrefix . $params['name'])) > 40) {
      return "Nome Grupo/Clan muito grande!";
    }

    // // Verificar por segurança se está mesmo no grupo para criar canais
    // if (!isWhiteListed($create_channels_group, getClientServerGroups($client))) {
    //   return "Não estás num grupo válido para criar canais.";
    // }

    //TODO: Spam prevention

    try {
      $userChannelID = $this->ts3->createUserChannel($spacerPrefix . $params['name'], $client);

      $user->channels()->create([
        'cid' => $userChannelID['top'],
        'channel_order' => 1,
        'status' => 'good',
        'channel_deleted' => 1 ]);

      $this->ts3->messageClient($client['clid'], "\n[b]Canal Criado! Diverte-te!!\nFoi-te dado Channel Admin, não te esqueças de alterar o nome e password das salas.[/b]");

      if ($params['move'] == 'on') {
        $this->ts3->moveUser($client['clid'], $userChannelID['first']);
      }
    } catch (TeamSpeak3_Exception $e) {
      echo $e->getTraceAsString();

    }
    echo "Yaay Created";
    return view('teamspeak.create');
  }

  public function deleteChannel() {

    return view('teamspeak.create');
  }

  public function createSubChannel() {

    // if (strlen(trim($_POST['sub_channel_name'])) < 1) {
    //   $errors[] = "Precisas de preencher todos os campos!";
    // }

    // // [cspacer0C] = prefixo dos spacers
    // if (strlen(trim($_POST['sub_channel_name']) . '[cspacer0C]') > 40) {
    //   $errors[] = "Nome muito grande!";
    // }

    // if (count($ts3->channelGetById($mainChannel)->subChannelList()) >= $sub_channel_limit) {
    //   $errors[] = "Não podes criar mais de $sub_channel_limit sub-canais!";
    // }

    // if (empty($errors)) {

    //   try {
    //     createSingleChannel($_POST['sub_channel_name'], $_POST['sub_channel_password'], $mainChannel);
    //   } catch (TeamSpeak3_Exception $e) {
    //     die('Erro, por favor reporta aos administradores: ' . $e->getMessage());
    //   }

    //   // Fix de não mostrar as atualizações
    //   header("Location: index.php");
    //   die();
    
    // }
  }

  public function deleteSubChannel() {
    $mainChannel = resolve('mainChannel');

    $params = request()->post();

    $cid = $params['cid'];

    //Does user have channel
    if ($mainChannel) {

      if ($mainChannel['cid'] != $cid && count($mainChannel->subChannelList()) == 1) {
        $errors[] = 'Não podes apagar um sub canal quando é o único restante.';
      }

      // Se for canal principal guardar na DB quando foi apagado
      if ($cid == $mainChannel) {
        try {
          $this->ts3lib->channelDelete($cid, true);
        } catch (TeamSpeak3_Exception $e) {
          die('Erro, por favor reporta aos administradores: ' . $e->getMessage());
        }
        return redirect('/');
      }

      try {
        $this->ts3->deleteMainChannel($cid);
      } catch (TeamSpeak3_Exception $e) {
        die('Erro, por favor reporta aos administradores: ' . $e->getMessage());
      }

      //TODO: set channel as deleted and save TimeStamp
      // saveChannelDeletedTimestamp($client['client_unique_identifier']);
    }
  }
}


  // public function store() {

  //   $pizza = new Pizza();

  //   $pizza->name = request('name');
  //   $pizza->type = request('type');
  //   $pizza->base = request('base');
  //   $pizza->toppings = request('toppings');

  //   $pizza->save();

  //   return redirect('/')->with('mssg', 'Thanks for your order!');

  // }

  // public function destroy($id) {

  //   $pizza = Pizza::findOrFail($id);
  //   $pizza->delete();

  //   return redirect('/pizzas');

  // }


