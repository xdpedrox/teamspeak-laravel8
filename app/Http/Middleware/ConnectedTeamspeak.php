<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Services\TeamSpeak;
use App\Models\Tsuser;
use App\Models\Channel;

class ConnectedTeamspeak
{
    public function __construct(TeamSpeak $ts3) {
      $this->ts3 = $ts3;
      $this->ts3lib = $ts3->ts3lib();
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
      // $client_ip = request()->ip();
      $client_ip= '129.12.164.134';

      //not connected
      if (($client = $this->ts3->find_client_by_ip($client_ip)) == false) {
        return response(view('teamspeak.not_connected'));
      }

      //Find user im the database
      $user = Tsuser::firstWhere([
        'unique_id' => $client['client_unique_identifier'], 
        'client_db_id' => $client['client_database_id']
        ]);
      
      // Create user if user is no the database 
      if (!$user) {
        //Create user
        $user = Tsuser::create([
          'unique_id'=> $client['client_unique_identifier'],
          'client_db_id' => $client['client_database_id'],
        ]);
      }

      app()->instance('tsuser', $user);
      app()->instance('tsclient', $client);

      $channels = $user->channels()->where('status', "good")->get()->toArray();

      // Shows channel if is present in database and in the ts server otherwise creaete page
      if (count($channels) > 0 && count($foundChannels = $this->ts3->channelsExist(array_column($channels, 'cid'))) > 0) { 
        app()->instance('channel', Channel::firstWhere('cid', $foundChannels[0]['cid']));
        app()->instance('mainChannel', $this->ts3lib->channelGetById($foundChannels[0]['cid']));
      }
      

      return $next($request);
    }
}
