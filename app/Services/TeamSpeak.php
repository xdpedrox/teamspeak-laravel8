<?php

namespace App\Services;

use GuzzleHttp\Client;
use App\Services\RequestHandler;
use App\Services\TeamSpeakCore;

use Teamspeak3;
use TeamSpeak3_Exception;
use TeamSpeak3_Node_Client;

class TeamSpeak
{
  protected $ts3;

  public function __construct() {
    $connection = config('teamspeak.connection');
    $url = "serverquery://$connection[user]:$connection[pass]@$connection[ip]:$connection[query_port]/?server_port=$connection[voice_port]&nickname=$connection[display_name]-" . mt_rand(10, 1000);
    try {
      $this->ts3lib = TeamSpeak3::factory($url);
    } catch (TeamSpeak3_Exception $e) {
      return "Error " . $e->getCode() . ": " . $e->getMessage();
      #throw $e;
    }
  }

  function test() {
    // echo \request()->ip();
    // // var_dump($this->ts3lib->channelGroupList());
    // // $this->messageClient(1, 'Hello');
    // $owner = $this->find_client_by_ip('129.12.164.134');
    // var_dump($owner);
    // if ($owner) {
    //     $this->createUserChannel('spacer-name', 'name', '', $owner);
    // } else {
    //   echo 'Nope';
    // }

  }

  // Encontrar cliente por IP
  function find_client_by_ip($ip){
    foreach ($this->ts3lib->clientList() as $client) {
      if ($client['connection_client_ip'] == $ip && $client['client_type'] == 0) {
        // Devolve instância de -> TeamSpeak3_Node_Client
        // Os métodos da página baixos serão os que vamos usar pelo cliente obtido nesta função
        // http://docs.planetteamspeak.com/ts3/php/framework/class_team_speak3___node___client.html
        return $client;
      }
    }
    return false;
  }

  // Devolve os server groups do cliente
  function getClientServerGroups(TeamSpeak3_Node_Client $client) {
    return explode(',', $client['client_servergroups']);
  }

  // Vê se o grupo está na array
  // Passar Id do grupo e uma array com Ids
  function isGroupListed($id, array $groups) {
    foreach ($groups as $g) {
      if ($g == $id) {
        return true;
      }
    }
    return false;
  }

  // Vê se vários grupos estão na array
  // Passar array com Ids dos grupos e uma array com Ids para comparar
  // retorna true na primeira match
  function areGroupsListed(array $groupids, array $groups) {
    foreach ($groupids as $g) {
      if ($this->isGroupListed($g, $groups)) return true;
    }
    return false;
  }

  // Pseudo hack para adaptar a vários grupos
  // $ids pode ser um número ou uma array com números (Ids de grupos)
  // -0 será para fazer whitelist a TUDO que é grupo
  function isWhiteListed($ids, array $groups) {
    if ($ids === 0) return true;

    return is_array($ids) ? $this->areGroupsListed($ids, $groups) : $this->isGroupListed($ids, $groups);
  }

  // procurar um canal por ID
  function getChannel($id)
  {
    foreach ($this->ts3lib->channelList() as $channel) {
      if ($channel['cid'] == $id) {
        // Devolve instância de -> TeamSpeak3_Node_Channel
        // Os métodos da página baixos serão os que vamos usar pelo cliente obtido nesta função
        // http://docs.planetteamspeak.com/ts3/php/framework/class_team_speak3___node___channel.html
        return $channel;
      }
    }
    return false;
  }

  function channelsExist($channel_ids)
  {
    $channelsFound = [];
    foreach ($this->ts3lib->channelList() as $channel) {
      foreach ($channel_ids as $id) {      
        if ($channel['cid'] == $id) {
          $channelsFound[] = $channel;
        }
      }
    }
    return $channelsFound;
  }

  // verificar se um canal existe
  // alias de getChannel mas devolve apenas true/false
  function channelExists($id)
  {
    return $this->getChannel($id) ? true : false;
  }

  // criar canal para utilizador
  // o owner apenas serve para atribuir channel admin
  function createUserChannel($spacer_name, TeamSpeak3_Node_Client $owner) {

    $config = config('teamspeak.config');;

    $lastId = null;
    $gotStart = false;
    foreach ($this->ts3lib->channelList() as $c) {

      if ($c['cid'] == $config['user_channels_start']) {
        $gotStart = true;
      }

      if (!$gotStart || $c['pid'] != 0) {
        continue;
      }

      if ($c['cid'] == $config['user_channels_end']) {
        break;
      }

      $lastId = $c['cid'];
    }

    if (!isset($lastId)) {
      return false;
    }


    $spacer_top_cid = $this->createUserTopSpacer($spacer_name, $lastId);

    $spacer_empty_line_cid = $this->ts3lib->channelSpacerCreate($this->spacerCrc32($spacer_name, time()), '', TeamSpeak3::SPACER_ALIGN_REPEAT, $spacer_top_cid);

    $spacer_line_cid = $this->ts3lib->channelSpacerCreate($this->spacerCrc32($spacer_name, time()), '━', TeamSpeak3::SPACER_ALIGN_REPEAT, $spacer_empty_line_cid);


    // Dar channel admin ao spacer com nome
    $this->ts3lib->clientSetChannelGroup($owner['client_database_id'], $spacer_top_cid, $config['channel_admin_group']);


    $firstChannelID = null;

    for ($i = 1; $i <= 3; $i++) {
      $subChannelId = $this->createSingleChannel("● Convivio $i", '', $spacer_top_cid);

      // Para a função devolver o ID do 1º canal, remover se a implementação do loop for removida.
      if (!isset($firstChannelID)) $firstChannelID = $subChannelId;
    }

    // $spacer_top_cid será o ID do canal a guardar na base de dados.
    return array('top' => $spacer_top_cid, 'first' => $firstChannelID);
  }

  // re utilizável para criar o canal principal e para os sub canais.
  // parentID = o ID do canal onde vamos criar este canal como sub.
  function createSingleChannel($name, $password, $parentID) {

    $config = config('teamspeak.config');;

    // global $codec;
    // global $codec_quality;

    $channelID = $this->ts3lib->channelCreate(array(
      "channel_name" => $name,
      "channel_password" => $password,
      "channel_flag_permanent" => TRUE,
      "cpid" => $parentID,
    ));

    $permid = array(
      'i_ft_needed_file_upload_power',
      'i_ft_needed_file_download_power',
      'i_ft_needed_file_delete_power',
      'i_ft_needed_file_rename_power',
      'i_ft_needed_file_browse_power',
      'i_ft_needed_directory_create_power',
      'i_channel_needed_permission_modify_power',
    );

    foreach ($permid as $perm) {
      $permvalue[] = 0;
    }


    $this->ts3lib->channelPermAssign($channelID, $permid, $permvalue);

    return $channelID;
  }

  function createUserTopSpacer($spacer_name, $order) {

    // TODO: Count channels 
    // Make a function that counts the number of channels and returns the next number.
    // Also changes the name of the channels in case the number doesn't match

    $ch = $this->ts3lib->execute("channelcreate", array(
      "channel_name" => $spacer_name,
      "channel_maxclients" => 0,
      "channel_flag_maxclients_unlimited" => FALSE,
      "channel_flag_maxfamilyclients_unlimited" => FALSE,
      "channel_flag_permanent" => TRUE,
      "channel_order" => $order,
    ))->toList();

    $permid = array(
      'i_ft_needed_file_upload_power',
      'i_ft_needed_file_download_power',
      'i_ft_needed_file_delete_power',
      'i_ft_needed_file_rename_power',
      'i_ft_needed_file_browse_power',
      'i_ft_needed_directory_create_power',
      'i_channel_needed_permission_modify_power',
    );

    foreach ($permid as $perm) {
      $permvalue[] = 0;
    }

    $this->ts3lib->channelPermAssign($ch['cid'], $permid, $permvalue);

    return $ch['cid'];
  }

  function isChannelOwned($channelToTest, $userMainChannelId)
  {

    if ($userMainChannelId == $channelToTest) return true;

    foreach ($this->ts3lib->channelGetById($userMainChannelId)->subChannelList() as $c) {
      if ($c['cid'] == $channelToTest) return true;
    }

    return false;
  }

  // Gera um ID para spacer do tipo 9349c6a9 que fica [*spacer9349c6a9]
  // como depois do 'r' do spacer não pode ter letra, adicionamos um número caso aconteça
  function spacerCrc32($spacerName, $uniqueNum)
  {
    $crc32 = dechex(crc32($spacerName . $uniqueNum));

    return ctype_alpha($crc32[0]) ? '0' . $crc32 : $crc32;
  }

  // $deletespacer = true apaga o canal e o spacer antes (ex: apagar o canal principal)
  function deleteMainChannel($cid) {

    $config = config('teamspeak.config');

    $spacerId = null;

    $found = false;

    // Loop sobre todos os canais.
    foreach ($this->ts3lib->channelList() as $getSpacer) {

      if ($found) {
        // Get the spacer channel object
        $spacer_obj = $this->ts3lib->channelGetById($getSpacer['cid']);

        // Se o canal for um spacer e se o canal nao tiver sub canais. e se nao for o ultimo canal
        if ($spacer_obj->isSpacer()) {
          if (count($spacer_obj->subChannelList()) == 0 && $getSpacer['cid'] != $config['user_channels_end']) {
            $this->ts3lib->channelDelete($getSpacer['cid'], true);
          } else {
            break;
          }
        }
      }

      // para o codigo quando atinge o canal do user.
      if ($getSpacer['cid'] == $cid) {
        $found = true;
      }
    }
  }

  function moveUser($clientID, $channelID)
  {

    $this->ts3lib->clientMove($clientID, $channelID);
  }

  function messageClient($clientID, $message)
  {

    $this->ts3lib->clientGetById($clientID)->message($message);
  }

  function ts3lib() {
    return $this->ts3lib;
  }

}
