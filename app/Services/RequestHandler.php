<?php

namespace App\Services;

use GuzzleHttp\Client;

class RequestHandler
{
  protected $url;
  protected $http;
  protected $headers;

  public function __construct(Client $client)
  {
    $this->url = 'http://eliteportuguesa.pt:10080/1/';
    $this->http = $client;
    $this->headers = [
      'x-api-key' => 'BACPjUnUDc4bAIrimLPKjAW44rhft0wu4_k6E2b'
    ];
  }

  function getResponse(string $uri = null, array $post_params = [])
  {
    $full_path = $this->url;
    $full_path .= $uri;
    var_dump($full_path);
    $request = $this->http->get($full_path, [
      'headers'         => $this->headers,
      // 'timeout'         => 30,
      // 'connect_timeout' => true,
      // 'http_errors'     => true,
      'query'     => $post_params
    ]);

    $response = $request ? $request->getBody()->getContents() : null;
    $status = $request ? $request->getStatusCode() : 500;

    if ($response && $status === 200 && $response !== 'null') {
      $object = (object) json_decode($response);

      if ($object->status->code == 0) {
        return $object->body;
      } else {
        return $object;
      }
    }

    return null;
  }

  function postResponse(string $uri = null, array $post_params = [])
  {
    $full_path = $this->url;
    $full_path .= $uri;

    $request = $this->http->post($full_path, [
      'headers'         => $this->headers,
      // 'timeout'         => 30,
      // 'connect_timeout' => true,
      // 'http_errors'     => true,
      'json'            => $post_params,
    ]);

    $response = $request ? $request->getBody()->getContents() : null;
    $status = $request ? $request->getStatusCode() : 500;

    if ($response && $status === 200 && $response !== 'null') {
      $object = (object) json_decode($response);

      if ($response->status->code == 0) {
        return $object->body;
      } else {
        return $object;
      }
    }

    return null;
  }

  public function get($url)
  {
    return $this->getResponse($url);
  }
}
