<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'cid',
      'status',
      'channel_order',
      'channel_deleted',
    ];

    public function tsusers() {
      return $this->belongsToMany(Tsuser::class)->withTimestamps();
    }
}
