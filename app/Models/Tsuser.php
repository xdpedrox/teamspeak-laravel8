<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tsuser extends Model
{
    use HasFactory;

    protected $fillable = [
      'unique_id',
      'client_db_id',
    ];

    public function channels() {
      return $this->belongsToMany(Channel::class)->withTimestamps();
    }
}
