@extends('layouts.layout')

@section('content')
<div>Create</div>

<form method="post" action="{{ route('teamspeak.createChannel') }}">

  <!-- CROSS Site Request Forgery Protection -->
  @csrf

  <div class="form-group">
      <label>Nome do teu clan / guild / grupo:</label>
      <input type="text" class="form-control" id="name" name="name">
  </div>

  <div class="form-group">
      <label>Move to Channel</label>
      <input type="checkbox" class="form-control" id="move" name="move">
  </div>

  <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
</form>
@endsection